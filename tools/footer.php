<div class="footer-copyright">
    © <?php echo date("Y")?> Tous droits réservés - Main Tendue 31
    <div class="footer-lastupdate">Dernière mise à jour 20-11-2018</div>
</div>
<div class="footer-mentions"><a href="/mentions">Mentions légales</a></div>
<div class="footer-credits"><span class="credits">Par Melody, KouassiWeb, David.B, Etienne.P</span></div>
<!--  
<div class="footer-credits">Par <span class="credits">Melody</span>, <span class="credits">KouassiWeb</span> et <span class="credits">David.B</span></div>
-->